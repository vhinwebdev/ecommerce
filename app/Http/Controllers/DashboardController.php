<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\OrderItem;
use DB;

class DashboardController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

        $orders = new OrderItem();


        return view('admin.dashboard')->with('bestsellers', $orders->select('*', DB::raw('sum(units) units'))->select('*', DB::raw('sum(price) price'))->groupBy('product_id')->get());

    }
}
