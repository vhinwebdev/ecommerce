<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Stripe\Stripe;
use App\Models\Order;
use Stripe\StripeClient;
use Session;

class PaymentController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function paymentSuccess()
    {
        $stripe = new StripeClient('sk_test_CmvIVgBRSleRQe7AiqEEn9nq');

        if(!session()->has('stripe_payment_intent')){
            return back();
        }
        
        try{
			
            $payment_intent = $stripe->paymentIntents->retrieve(
                session()->get('stripe_payment_intent')
            );
            
			$data=$payment_intent['charges']['data'][0];
			$charge_id=$data['id'];
				
            session()->forget('stripe_payment_intent');

            session()->put('payment_method',['name'=>'stripe','id'=>$charge_id]);
            
            $updateOrder = Order::find(session()->get('order_id'));
            $updateOrder->payment_status = 1;
            $updateOrder->payment_method = 'stripe';
            $updateOrder->save();

            //Clear cart
            $cart = new CartController;
            $cart->clearCart();

            session()->forget('order_id');

            return view('confirmation')->with('success', 'success');
        }
        catch(\Exception $e){
            return back()->withErrors('Error, try again');
        }
    }

    public function confirmation()
    {
        return view('confirmation');
    }
  
    public function makePayment(Request $request)
    {
        $stripe = new \Stripe\StripeClient('sk_test_CmvIVgBRSleRQe7AiqEEn9nq');

        $cart = session()->get('cart');
        $lineItems = array();

        foreach( $cart as $item ){

            $lineItems[] = [
                'price_data' => [
                    'currency' => 'usd',
                'product_data' => [
                    'name' => $item['name'],
                ],
                'unit_amount' => (int)($item['price'] * 100),
                ],
                'quantity' => $item['units'],

            ];

        }

        

        $session = $stripe->checkout->sessions->create([
            'payment_method_types' => ['card'],
            'line_items' => $lineItems,
            'mode' => 'payment',
            'success_url' => route('checkout.confirm'),
            'cancel_url' => route('checkout.index'),
          ]);
          
        //return back()->with('id', $session->id);

        session()->put('stripe_payment_intent',$session->payment_intent);

        return response()->json(['id' => $session->id], 200);
    }
}
