<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CartController;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Order;
use App\Models\OrderItem;
use Session;
use Auth;

class CheckoutController extends Controller
{

    public function index()
    {
        $isUserLoggedIn = '';
        if( Auth::check() ){
            $isUserLoggedIn = Auth::user();
        }

        return view('checkout')->with('user', $isUserLoggedIn);
    }

    public function storeOrderDetails($params){

        $cart = new CartController;


        $order = Order::create([
            'order_number'      =>  'ORD-'.strtoupper(uniqid()),
            'user_id'           => auth()->user()->id,
            'status'            =>  'pending',
            'grand_total'       =>  $cart->getSubTotal(),
            'item_count'        =>  $cart->getTotalQuantity(),
            'payment_status'    =>  'pending',
            'payment_method'    =>  null,
            'first_name'        =>  $params['first_name'],
            'last_name'         =>  $params['last_name'],
            'address'           =>  $params['address'],
            'city'              =>  $params['city'],
            'country'           =>  $params['country'],
            'post_code'         =>  $params['post_code'],
            'phone_number'      =>  $params['phone_number'],
            'notes'             =>  $params['notes']
        ]);
    
        if ($order) {
            
            $cart = new CartController;
            $items = $cart->getContent();
            
            foreach ($items as $item)
            {
                // A better way will be to bring the product id with the cart items
                // you can explore the package documentation to send product id with the cart
                $product = Product::where('name', $item['name'])->first();
                
                $orderItem = new OrderItem([
                    'product_id'    =>  $product['id'],
                    'units'      =>  $item['units'],
                    'price'         =>  $item['price'] * $item['units']
                ]);
    
                $order->items()->save($orderItem);
            }
        }
    
        return $order;

    }

    public function placeOrder(Request $request)
    {
        // Before storing the order we should implement the
        // request validation which I leave it to you
        $order = $this->storeOrderDetails($request->all());

        session()->put('order_id', $order->id);

        //Clear cart contents
        if( (bool) $order ){
            

            return response()->json(['status' => (bool) $order], 200);
        }
        
    }
}
