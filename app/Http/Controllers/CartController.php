<?php

namespace App\Http\Controllers;
use App\Models\Product;
use Illuminate\Http\Request;
use Session;

class CartController extends Controller
{

    public function index()
    {
        return view('cart')->with('carts', '');
    }

    public function getContent()
    {
        return Session::get('cart');
    }

    public function clearCart()
    {
        Session::pull('cart', '');
    }

    public function getSubTotal()
    {
        $subTotal = 0;

        foreach( Session::get('cart') as $id => $details ){
            $subTotal += $details['price'] * $details['units'];
        }

        return $subTotal;
    }

    public function getTotalQuantity()
    {
        $total = 0;

        foreach( Session::get('cart') as $id => $details ){
            $total += $details['units'];
        }

        return $total;
    }

    public function store($id)
    {
        $product = Product::find($id);

        if(!$product) {

            abort(404);

        }

        $cart = session()->get('cart');

        // if cart is empty then this the first product
        if(!$cart) {

            $cart = [
                    $id => [
                        "name" => $product->name,
                        "units" => 1,
                        "description" => $product->description,
                        "price" => $product->price,
                        "photo" => $product->image
                    ]
            ];

            session()->put('cart', $cart);

            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }

        // if cart not empty then check if this product exist then increment units
        if(isset($cart[$id])) {

            $cart[$id]['units']++;

            session()->put('cart', $cart);

            return redirect()->back()->with('success', 'Product added to cart successfully!');

        }

        // if item not exist in cart then add to cart with units = 1
        $cart[$id] = [
            "name" => $product->name,
            "units" => 1,
            "price" => $product->price,
            "description" => $product->description,
            "photo" => $product->image
        ];

        session()->put('cart', $cart);

        return redirect()->back()->with('success', 'Product added to cart successfully!');

    }

    public function update(Request $request)
    {
        if($request->id and $request->units)
        {
            $cart = session()->get('cart');

            $cart[$request->id]["units"] = $request->units;

            session()->put('cart', $cart);

            session()->flash('success', 'Cart updated successfully');
        }
    }

    public function remove(Request $request)
    {
        if($request->id) {

            $cart = session()->get('cart');

            if(isset($cart[$request->id])) {

                unset($cart[$request->id]);

                session()->put('cart', $cart);
            }

            session()->flash('success', 'Product removed successfully');
        }
    }
}
