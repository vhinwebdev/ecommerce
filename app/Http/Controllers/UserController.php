<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    //Returns all users with their orders
    public function index()
    {
        //return response()->json(User::with(['orders'])->get());

        return view('admin.users.index')->with('users', User::get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('admin.users.edit', compact('user'));
    }

    //Creates a user account, authenticates it and generates an access token
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:50',
            'email' => 'required|email',
            'password' => 'required|min:8|confirmed',
        ]);

        if ($validator->fails()) {
            //return response()->json(['error' => $validator->errors()], 401);

            return redirect('user/create')
                        ->withErrors($validator)
                        ->withInput();

        }

        $data = $request->only(['name', 'email', 'password']);
        $data['password'] = bcrypt($data['password']);

        $user = User::create($data);
        $user->is_admin = 0;

        // return response()->json([
        //     'user' => $user,
        //     'token' => $user->createToken('bigStore')->accessToken,
        // ]);

        return redirect()->route('user.index')->with('user', $user)->with('token', $user->createToken('bigStore')->accessToken)->with('message', $user ? 'User Created!' : 'Error Creating User' )->with('status', (bool) $user);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('user/'.$user->id.'/edit')
                        ->withErrors($validator)
                        ->withInput();
        }


        $userData = $request->only(['name', 'first_name', 'last_name', 'address', 'city', 'country', 'post_code', 'phone_number', 'password' ]);

        $userData['password'] = Hash::make($userData['password']);

        $status = $user->update( $userData );

        return redirect()->route('user.index')->with('status', $status)->with('message', $status ? 'User Updated!' : 'Error Updating User');

    }


     /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $status = $user->delete();

        return redirect()->route('user.index')->with('status', $status)->with('message', $status ? 'User Deleted!' : 'Error Deleting User');
  
    }

    //Authenticate a user and generates an access token for that user
    public function login(Request $request)
    {
        $status = 401;
        $response = ['error' => 'Unauthorised'];

        if (Auth::attempt($request->only(['email', 'password']))) {
            $status = 200;
            $response = [
                'user' => Auth::user(),
                'token' => Auth::user()->createToken('bigStore')->accessToken,
            ];
        }

        return response()->json($response, $status);
    }

    
    //Get details of user and returns a json
    public function show(User $user)
    {
        return response()->json($user);
    }

    //Get all orders for a user and returns a json
    public function showOrders(User $user)
    {
        return response()->json($user->orders()->with(['product'])->get());
    }
}
