@extends('layouts.app') @section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session("success") }}
            </div>
            @endif
            <div class="row">
                <div class="col-md-5 text-center">
                    <img src="{{ $product->image }}" class="img-fluid" />
                </div>
                <div class="col-md-7">
                    <h4>{{ $product->name }}</h4>
                    <p>{{ $product->price }}</p>
                    <p>{{ $product->description }}</p>

                    <a
                        class="btn btn-primary"
                        href="{{ route('cart-add', $product->id) }}"
                        role="button"
                        >Add to cart</a
                    >
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
