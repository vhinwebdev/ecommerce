<div class="col-md-2">
    <a class="btn btn-primary btn-block btn-dark" href="/dashboard" role="button">Dashboard</a>
    @if (Auth::user()->is_admin)
        <a class="btn btn-primary btn-block btn-dark" href="/product" role="button">Products</a>
        <a class="btn btn-primary btn-block btn-dark" href="/order" role="button">Orders</a>
        <a class="btn btn-primary btn-block btn-dark" href="/user" role="button">Customers</a>
    @else
        <a class="btn btn-primary btn-block btn-dark" href="/order" role="button">Order History</a>
    @endif
</div>
