@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2>Edit order</h2>
                </div>
                <div class="pull-right py-3">
                    <a class="btn btn-primary" href="{{ route('order.index') }}" title="Go back"> Go back </a>
                </div>
            </div>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{ route('order.update', $order->id) }}" method="POST">
            @csrf
            @method('PUT')

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <header class="card-header">
                            <h4 class="card-title mt-2">Order Details</h4>
                        </header>
                        <article class="card-body">

                            <div class="form-row">
                                <div class="col form-group">
                                    <label>Order Number</label>
                                    <p>{{ $order->order_number }}</p>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col form-group">
                                    <label>Payment Status</label>
                                    <p>{{ $order->payment_status }}</p>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col form-group">
                                    <label>Payment Method</label>
                                    <p>{{ $order->payment_method }}</p>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col form-group">
                                    <label>Total Items</label>
                                    <p>{{ $order->item_count }}</p>
                                </div>
                            </div>




                            <div class="form-group">
                                <label>Order Status</label>
                                <select class="form-control" name="status">
                                    <option value="pending" @if ($order->status === 'pending') selected @endif>Payment Pending</option>
                                    <option value="processing" @if ($order->status === 'received') selected @endif>Payment Received</option>
                                    <option value="completed" @if ($order->status === 'completed') selected @endif>Completed</option>
                                    <option value="shipped" @if ($order->status === 'shipped') selected @endif>Shipped</option>
                                    <option value="decline" @if ($order->status === 'decline') selected @endif>Decline</option>
                                </select>
                            </div>


                            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </article>
                    </div>
                </div>
            </div>


        </form>
    </div>
@endsection
