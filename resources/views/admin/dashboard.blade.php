@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">

            @include('admin.nav')

            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">{{ __('Dashboard') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <table class="table">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">Product</th>
                                    <th scope="col">Total Sold</th>
                                    <th scope="col">Sales</th>
                                    <th scope="col">Net Profit</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($bestsellers as $item)
                                    <tr>
                                        <th scope="row">{{ $item->product_id }}</th>
                                        <td>{{ $item->units }}</td>
                                        <td>{{ $item->price }}</td>
                                        <td>{{ $item->price }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
