@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2>Edit user</h2>
                </div>
                <div class="pull-right py-3">
                    <a class="btn btn-primary" href="{{ route('user.index') }}" title="Go back"> Go back </a>
                </div>
            </div>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{ route('user.update', $user->id) }}" method="POST">
            @csrf
            @method('PUT')

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <header class="card-header">
                            <h4 class="card-title mt-2">Personal Details</h4>
                        </header>
                        <article class="card-body">

                            <div class="form-row">
                                <div class="col form-group">
                                    <label>Name</label>
                                    <input type="text" class="form-control" name="name" value="{{ $user->name }}"
                                        required>
                                </div>
                            </div>

                            <div class=" form-row">
                                <div class="col form-group">
                                    <label>First name</label>
                                    <input type="text" class="form-control" name="first_name"
                                        value="{{ $user->first_name }}" required>
                                </div>
                                <div class="col form-group">
                                    <label>Last name</label>
                                    <input type="text" class="form-control" name="last_name"
                                        value="{{ $user->last_name }}" required>
                                </div>
                            </div>


                            <div class="form-group">
                                <label>Address</label>
                                <input type="text" class="form-control" name="address" value="{{ $user->address }}"
                                    required>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>City</label>
                                    <input type="text" class="form-control" name="city" value="{{ $user->city }}"
                                        required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Country</label>
                                    <input type="text" class="form-control" name="country" value="{{ $user->country }}"
                                        required>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group  col-md-6">
                                    <label>Post Code</label>
                                    <input type="text" class="form-control" name="post_code"
                                        value="{{ $user->post_code }}" required>
                                </div>
                                <div class="form-group  col-md-6">
                                    <label>Phone Number</label>
                                    <input type="text" class="form-control" name="phone_number"
                                        value="{{ $user->phone_number }}" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Email Address</label>
                                <input type="email" class="form-control" name="email" value="{{ auth()->user()->email }}"
                                    disabled>
                                <small class="form-text text-muted">We'll never share your email with anyone
                                    else.</small>
                            </div>

                            <div class="form-group">
                                <strong>Password:</strong>
                                <input type="text" name="password" class="form-control" value="" placeholder="Password">
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </article>
                    </div>
                </div>
            </div>


        </form>
    </div>
@endsection
