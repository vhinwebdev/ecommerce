@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h4>{{ __('Catalog') }}</h4>

                @if (session('success'))
                    <div class="alert alert-success" role="alert">
                        {{ session('success') }}
                    </div>
                @endif

                <div class="row row-cols-1 row-cols-md-3 row-cols-lg-4">
                    @foreach ($products as $product)


                        <div class="col-md-4">
                            <div class="card mb-4 shadow-sm">
                                <img class="card-img-top" data-src="{{ $product->image }}" alt="Thumbnail [100%x225]"
                                    style="height: 225px; width: 100%; display: block;" src="{{ $product->image }}"
                                    data-holder-rendered="true">
                                <div class="card-body">
                                    <h5 class="card-title">{{ $product->name }}</h5>
                                    <p class="card-text">{{ $product->description }}<br />{{ config('app.currency') }}
                                        {{ $product->price }}</p>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div class="btn-group">
                                            <a class="btn btn-sm btn-outline-secondary"
                                                href="{{ route('product.show', $product->id) }}">View</a>
                                            <a class="btn btn-sm btn-outline-secondary"
                                                href="{{ route('cart-add', $product->id) }}" role="button">Add to
                                                cart</a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    @endforeach

                </div>

            </div>
        </div>
    </div>
@endsection
