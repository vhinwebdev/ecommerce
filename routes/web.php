<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\PaymentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
Route::resource('product', ProductController::class);
Route::resource('order', OrderController::class);
Route::resource('user', UserController::class);
Route::resource('cart', CartController::class);

//Product
Route::get('/products/{id}', [ProductController::class, 'show']);

//Cart
Route::get('/cart', [CartController::class, 'index']);
Route::get('add-to-cart/{id}', [CartController::class, 'store'])->name('cart-add');
Route::patch('update-cart', [CartController::class, 'update'])->name('cart-update');
Route::delete('remove-from-cart', [CartController::class, 'remove'])->name('cart-remove');


Route::group(['middleware' => ['auth']], function () {
    Route::get('/checkout', [CheckoutController::class, 'index'])->name('checkout.index');
    Route::post('/checkout/order', [CheckoutController::class, 'placeOrder'])->name('checkout.place.order');

    
    //Payment
    Route::get('stripe', [PaymentController::class, 'index']);
    Route::post('create-checkout-session', [PaymentController::class, 'makePayment']);

});


Route::get('/confirmation', [PaymentController::class, 'paymentSuccess'])->name('checkout.confirm');
