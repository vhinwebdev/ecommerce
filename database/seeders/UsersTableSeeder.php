<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->name = "Alvin";
        $user->email = "alvin@balena.com";
        $user->password = bcrypt('12345678');
        $user->is_admin = true;
        $user->save();
        
    }
}
